'''
Created on Dec 13, 2018

@author: lucafavalli
'''
import simulation.jackson
import fcntl
import os

filepath = simulation.jackson.my_path + "/test/csv/classification_data/net_cls.csv"
sync_file = open(filepath, "w+")
#sync_file.filename = name

'''
Lock the access to the file.
'''
sync_file.lock = lambda: fcntl.lockf(sync_file, fcntl.LOCK_EX)

'''
Unlock the access to the file.
'''
sync_file.unlock = lambda: fcntl.lockf(sync_file, fcntl.LOCK_UN)

'''
List of objects observing the file updates.
''' 
sync_file.observers = []


def __add_observer(fl, observer):
    '''
    Adds an observer to the observers list of the file.
    
    Parameters
    ----------
    fl: File
        The observed file
    
    observer: any object with an update method
        The new file observer
    '''
    fl.lock()
    fl.observers.append(observer)
    fl.unlock()

sync_file.add_observer = lambda observer: __add_observer(sync_file, observer)

def __notify_observers(fl):
    '''
    Notifies the observers the file has been updated.
    
    Parameters
    ----------
    fl: File
        The observed file
    '''
    for observer in fl.observers:
        observer.update()

sync_file.notify_observers = lambda: __notify_observers(sync_file)

def __write_line(fl, line):
    '''
    Writes a new line to the file.
    
    Parameters
    ----------
    fl: File
        The file the line is being written to.
    
    Line: string
        The line to be written
    '''
    fl.lock()
    fl.write(line + os.linesep)
    fl.unlock()
    fl.notify_observers()
 
sync_file.write_line = lambda line: __write_line(sync_file, line)

def __read_line(fl, n_lines_from_last):
    '''
    Reads a line from the file, starting from the bottom.
    
    Parameters
    ----------
    fl: File
        The file to read.
    
    n_lines_from_last: int
        Counts how many lines should the reader should skip from the bottom.
        
    Returns
    ----------
    s: string
        The read line
    '''
    fl.seek(0)
    fl.lock()
    s = fl.read().splitlines()[-(n_lines_from_last + 1)]
    fl.unlock()
    return s

sync_file.read_line = lambda n = 0: __read_line(sync_file, n)