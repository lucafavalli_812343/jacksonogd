'''
Created on Dec 13, 2018

@author: lucafavalli
'''
import numpy as np
from sklearn.linear_model import SGDClassifier
from pandas.io.parsers import read_csv
import os
from cgi import escape
from numpy import indices
from scipy.linalg.tests.test_fblas import accuracy
import sklearn

def __sequential_update(observer, x, y):
    '''
    Update the observer in sequential (one example) mode
    
    Parameters
    ----------
    observer: DataObserver
        The observer to be updated
    x: array-like
        The new data x
    y: array-like
        The new data y
    '''
    try:
        #self.classifier.partial_fit(self.train_x, self.train_y, classes = self.classes)    
        simulated = observer.classifier.predict([x])[0]
        observer.tried += 1
        escape = '\33[31m'
        if simulated == y:
            observer.success += 1
            escape = '\33[32m'
        print('{}OGD - Expected {}; got {}; tried = {}; succeeded = {}; accuracy = {}\33[0m'.format(escape, y, simulated, observer.tried, observer.success, observer.success/observer.tried))
        with open(observer.output_file, 'a') as f:
            f.write('\n{:.8f}'.format(observer.success/observer.tried))
        
        observer.classifier.partial_fit([x], [y], classes = observer.classes)
        observer.train_x = np.append(observer.train_x, [x], axis = 0)
        observer.train_y = np.append(observer.train_y, [y], axis = 0)
    except AttributeError as e:
        print('FAILED FINDING CLASSIFIER')
        observer.train_x = np.array([x])
        observer.train_y = np.array([y])
        observer.classifier = SGDClassifier(loss="hinge", penalty="l2", max_iter=5)
        observer.classifier.partial_fit(observer.train_x, observer.train_y, classes=observer.classes)

def __retry_update(observer, x, y):
    '''
    Update the observer in retry mode
    To be used only in training set mode
    
    Parameters
    ----------
    observer: DataObserver
        The observer to be updated
    x: array-like
        The new data x
    y: array-like
        The new data y
    '''
    try:
        if(len(observer.train_x_errors) < 10):
            observer.mode = 'sequential'
            modes[observer.mode](observer, x, y)
        else:
            observer.train_x = np.append(observer.train_x, [x], axis = 0)
            observer.train_y = np.append(observer.train_y, [y], axis = 0)
            predictions = observer.classifier.predict(observer.train_x_errors)
            print('P -> {}     A -> {}'.format(len(predictions), len(observer.train_y_errors)))
            indices = [i for i in range(len(predictions)) if predictions[i] != observer.train_y_errors[i]]
            observer.train_x_errors = [observer.train_x_errors[i] for i in indices]
            observer.train_y_errors = [observer.train_y_errors[i] for i in indices]
            observer.classifier.partial_fit(observer.train_x_errors, observer.train_y_errors, classes = observer.classes)
            escape = '\33[32m'
            accuracy = 1 - (len(indices) / len(predictions))
            print('{}OGD - tried = {}; success = {}; accuracy = {}\33[0m'.format(escape, len(predictions), len(indices), accuracy))
            with open(observer.output_file, 'a') as f:
                f.write('\n{:.8f}'.format(accuracy))
    except (AttributeError, sklearn.exceptions.NotFittedError) as e:
        observer.train_x = np.append(observer.train_x, [x], axis = 0)
        observer.train_y = np.append(observer.train_y, [y], axis = 0)
        observer.classifier = SGDClassifier(loss="hinge", penalty="l2", max_iter=5)
        observer.classifier.partial_fit(observer.train_x, observer.train_y, classes = observer.classes)
        predictions = observer.classifier.predict(observer.train_x)
        indices = [i for i in range(len(predictions)) if predictions[i] != observer.train_y[i]]
        observer.train_x_errors = [observer.train_x[i] for i in indices]
        observer.train_y_errors = [observer.train_y[i] for i in indices]
        

modes = {'sequential': __sequential_update, 'retry': __retry_update}    

class DataObserver(object):
    '''
    An observer class for the Jackson Network data file.
    '''

    def __init__(self, fl, classes, training = None, mode = 'sequential'):
        '''
        Initialize the DataObserver
        
        Parameters
        ----------
        fl: File
            The observed file
        classes: array-like
            The available classifications
        training: file name
            Initial training dataset
        mode: string
            Learning mode for the SGD classifier
        '''
        self.observed = fl
        self.classes = classes
        self.tried = 0
        self.success = 0
        self.mode = mode
        if training is not None :
            data = read_csv(training, sep=',', header = 0).values
            data = data[~np.isnan(data).any(axis=1)]
            self.train_x = data[:,1:-1]
            self.train_y = np.array([row[-1] for row in data])
            print('Using a training set with {} examples'.format(np.size(data, 0)))
            self.classifier = SGDClassifier(loss="hinge", penalty="l2", max_iter=5)
            self.classifier.partial_fit(self.train_x, self.train_y, classes = self.classes)
        self.output_file = os.getcwd() + '/test/csv/classification_data/prediction_accuracy_timeline.csv'
        open(self.output_file, 'w').close()
        fl.add_observer(self)
        
    def update(self):
        '''
        The action to take when the observed file is updated (uses notify_observers).
        '''
        line = self.observed.read_line()
        line = line.split(',')
        new_x = list(map(float, line[1:-1]))
        new_y = int(line[-1])
        modes[self.mode](self, new_x, new_y)
        