'''
Created on Nov 14, 2018

@author: lucafavalli
'''
import numpy as np 
from scipy.stats import norm
import math

def generate_function(parameters, num = 1000):
    '''
    Generate a function (list of y-axis values) as sum of Gaussians.
    The i-th Gaussian curve is right shifted by i units and scaled according to the corresponding parameter.
    The result is normalized by dividing by the function maximum to obtain values in the [0, 1] interval. 
    
    Parameters
    ----------
    parameters: list
        parameters used to generate each Gaussian
    num: int
        Number of generated values on the x-axis
        
    Returns
    ----------
    f: array_like
        The y-axis values f of the generated function
    '''
    sz = len(parameters)
    x = np.linspace(0, sz, num)
    y0 = [norm.pdf(x, j/sz, parameter) for j, parameter in enumerate(parameters)]
    y0 = [y for y in y0 if not any([math.isnan(n) for n in y])]
    y = np.sum(y0, axis=0)
    #y = np.sum([norm.pdf(x, j/sz, parameter) for j, parameter in enumerate(parameters)], axis=0)
    return y / np.max(y)

def integrate(f, xrange = [0, 1]):
    '''
    Computes the integral of function f over the xrange interval.
    
    Parameters
    ----------
    f: list
        List of the available y-axis values of the function
    xrange: list
        Interval the function is defined on. Any value other then the first two is ignored 
        
    Returns
    ----------
    i: float
        The computed integral of the function    
    '''
    assert(xrange[0] < xrange[1])
    return np.sum(f*((xrange[1] - xrange[0]) / len(f)))

def distance(f1, f2):
    '''
    Computes a measure of the distance between two curves as the integral of the absolute value of the difference between the two functions.
    
    Parameters
    ----------
    f1, f2: list
        The two functions to be compared  
    Returns
    ----------
    i: float
        The computed distance between the functions
    '''
    assert(len(f1) == len(f2))
    return integrate(np.abs(np.subtract(f1, f2)))

def sigmoid(x):
    '''
    Computes the logistic sigmoid function on data x
    
    Parameters
    ----------
    f1: float
        The x value
    
    Returns
    ----------
    The computed sigmoid function over x
    '''
    return 1 / (1 + math.exp(-x))

def f_sparse(x, magic = 0.11443464630702983, ex=6):
    '''
    Spread the data
    '''
    #return min(1, max(0, (x + 1 - magic)**ex -1 + magic))
    return min(1, max(0, (x + 1 - magic)**ex -0.5))