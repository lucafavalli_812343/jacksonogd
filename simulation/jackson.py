'''
Created on Nov 19, 2018

@author: lucafavalli
'''
import os
import sys
import re
import time
from _datetime import datetime
from fileinput import filename

project_name = 'jacksonOGD'
my_path = os.getcwd()
dir_index = [m.start() for m in re.finditer(project_name, my_path)]
my_path = my_path[:dir_index[-1] + len(project_name)]
sys.path.append(my_path)
os.chdir(my_path)

from data_structures import network
from serialization import csv_network, sync_classification
from simulation.ogd import data_observer
import learning.tests as lt

from pynput import keyboard

def on_press(key):
    pass

def on_release(key):
    try:
        if key.char == 'q':
            print('\n\nReceived STOP signal...\n')
            print('Stopping Jackson network gently...\n\n')
            # Stop listener
            return False
    except AttributeError:
        pass
    
if __name__ == '__main__':
    start_time = time.time()
    t = 0
    try:
        t = int(sys.argv[1])
    except (IndexError, ValueError):
        t = 30
    directory = my_path + '/test/csv/net_csv_data'
    filename = '4c2ccdd17248034f07f5bde198607d612d6f261d'
    #filename = '825152a425d8e867a4ebb38b8e120a12a6792a4d'
    node_list = csv_network.read_nodes_from_csv(directory + '/' + filename)
    #observer = data_observer.DataObserver(sync_classification.sync_file, range(len(node_list)))
    #observer = data_observer.DataObserver(sync_classification.sync_file, range(len(node_list)), training = my_path + '/test/csv/classification_data/dataset/training_set.csv', mode='retry')
    observer = data_observer.DataObserver(sync_classification.sync_file, range(len(node_list)))
    #observer = data_observer.DataObserver(sync_classification.sync_file, range(len(node_list)), training = my_path + '/test/csv/classification_data/dataset/training_set.csv')

    my_net = network.Network(node_list)
    print(my_net)
    my_net.start()
    #time.sleep(t)

    print('Press Q to stop...\n')
    # Collect events until released
    with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()
    my_net.stop_gently()
    sync_classification.sync_file.close()
    
    total_time = int(time.time() - start_time)
    m, s = divmod(total_time, 60)
    h, m = divmod(m, 60)
    print('Total execution time: {} seconds = {}:{:02.0f}:{:02.0f}"'.format(total_time, h, m, s))
    '''
    #print(my_net.str_classifications())
    if 'windows' in os.name.lower():
        _ = os.system('cls')
    else:
        _ = os.system('clear')
    training_set_size = input('\nSize for the training set (total: {}) = '.format(observer.train_y.shape[0]))
    training_set_size = int(''.join(digit for digit in training_set_size if digit.isdigit()))
    assert training_set_size < observer.train_y.shape[0], 'Size of the training set must be less than the total available data.'
    
    train_x = observer.train_x[:training_set_size, :]
    train_y = observer.train_y[:training_set_size]
    
    test_x = observer.train_x[training_set_size:, :]
    test_y = observer.train_y[training_set_size:]
    lt.run(train_x, train_y, test_x, test_y)
    '''
    