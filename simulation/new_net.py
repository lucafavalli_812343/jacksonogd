'''
Created on Feb 11, 2019

@author: lucafavalli
'''

import os
import sys
import re

project_name = 'jacksonOGD'
my_path = os.getcwd()
dir_index = [m.start() for m in re.finditer(project_name, my_path)]
my_path = my_path[:dir_index[-1] + len(project_name)]
sys.path.append(my_path)
os.chdir(my_path)

from serialization import csv_network
from data_structures import network

if __name__ == '__main__':
    assert(len(sys.argv) == 3)
    directory = my_path + '/test/csv/net_csv_data'
    filename = csv_network.serialize_network(network.generate(int(sys.argv[1]), int(sys.argv[2])), directory)
    print('Net saved to {}'.format(filename))