'''
Created on Oct 26, 2018

@author: lucafavalli
'''
from data_structures import customer
import numpy as np
import threading
import time
import simulation.function_generator as fg
import random

class CustomerGenerator(threading.Thread):
    '''
    CustomerGenerator for a Network Facility.
    Each Facility has an input process that generates Customers and put them into the input queue.
    
    Class Attributes
    ----------
    customer_id: int
        Unique identifier for a new customer
    '''
    customer_id = 1000
    

    def __init__(self, facility, distribution = lambda: np.random.exponential(10), size = 50):
        '''
        Initialize the a CustomerGenerator object.
        
        Parameters
        ----------
        facility : Facility
            Facility the CustomerGenerator generates customers for
        distribution : function
            Distribution of interarrival times
        size : int
            The number of the parameters for the new customer
        '''
        super(CustomerGenerator, self).__init__()
        self.facility = facility
        self.distribution = distribution
        self.size = size
        
    def generate_customer(self):
        '''
        Create a new Customer to put inside the Facility incoming_customers queue.
        '''
        content = [random.uniform(0, 1) for _ in range(self.size)]
        self.facility.incoming_customers.put(customer.Customer(customer_id = CustomerGenerator.customer_id, content = content, sender = self.facility))
        CustomerGenerator.customer_id += 1
    
    def find_destination(self, customer, sort_function = None):
        '''
        Extract the next destination for the given customer by comparing his parameters to the facility ones.
        
        Parameters
        ----------
        customer: Customer
            The customer to redirect
        sort_function: function
            The function used to sort the facilities
            
        Parameters
        ----------
        facility: NetworkFacility
            The destination facility. If the result corresponds to the current facility the customer should leave the Network.
        '''
        sort_function = sort_function is None and self.default_key or sort_function
        '''
        print("FUNCTION")
        print(len(self.facility.parameters))
        print(len(fg.generate_function(self.facility.parameters)))
        '''
        p = fg.distance(fg.generate_function(customer.content), fg.generate_function(self.facility.parameters))
        
        #with open('p.csv', 'a') as f_out:
        #   f_out.write('{}\n'.format(p))
        #print("PARAMETER {} - SIGMOID {}".format(p, fg.sigmoid(p)))
        p = fg.f_sparse(p)
        n = len(self.facility.network.facilities) - 1
        print("PARAMETERS {}, {}".format(n, p))
        '''
        others = [self.facility.get_facility(i) for i in range(n) if i != self.facility.facility_id]
        facilities = [self.facility.get_facility(self.facility.facility_id)]
        facilities += sort_function is None and others or sorted(others, key = sort_function)
        '''
        facilities = [self.facility.get_facility(i) for i in range(n+1)]
        facility = facilities[np.random.binomial(n, p)]
        return facility['facility']
     
    def default_key(self, facility):
        '''
        The default function key for facilities sorting:
        you first sort by weighted queue length then by id
        '''
        #return (facility['facility'].incoming_customers.qsize() * facility['weight'], facility['facility'].facility_id)
        return facility['facility'].facility_id
        
    def run(self):
        '''
        The Generator behavior
        '''
        while self.facility.network.running:
            #time.sleep(0.1)
            time.sleep(self.distribution())
            self.generate_customer()
            
           