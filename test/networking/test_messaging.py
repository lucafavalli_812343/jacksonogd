'''
Created on Oct 26, 2018

@author: lucafavalli
'''
import data_structures.customer
from queue import Queue
from simulation import customer_generator
from types import FunctionType, BuiltinFunctionType
import numbers
from test.jackson_ogd_test import AbstractJacksonUnitTest

class TestMessaging(AbstractJacksonUnitTest):
    '''
    Unit Test Class for creating, sending and receiving messages from a facility to another.
    
    Required Features:
    - Message class
    - Message creation
    - Facility queues
    - Message semantic
    - Association between a customer and a message
    '''
    
    def testCreateMessage(self):
        '''
        Test the customer is correctly created
        '''
        customer = data_structures.customer.Customer(0, range(10))
        assert(customer.sender is None)
        assert(customer.receiver is None)
        assert(customer.age == 0)
        assert(customer.content == range(10))

    def testQueue(self):
        '''
        Test any new Facility also has a queue for incoming customers
        '''
        for node in self.node_list:
            try:
                assert(isinstance(node.incoming_customers, Queue))
            except Exception:
                assert(False)
                      
    def testCustomerGenerator(self):
        '''
        Test each new Facility also has a CustomerGenerator for new Customers.
        The CustomerGenerator should always refer to the Facility it is part of.
        '''
        for node in self.node_list:
            try:
                assert(isinstance(node.generator, customer_generator.CustomerGenerator))
                assert(node.generator.facility is node)
            except Exception:
                assert(False)          
    
    def testCustomerGeneration(self):
        '''
        The Customer Generator can create new Customers and put them inside the Facility Queue
        '''
        for node in self.node_list:
            n = node.incoming_customers.qsize()
            node.generator.generate_customer()
            assert(node.incoming_customers.qsize() > n)
    
    def testCustomerGenerationFacility(self):
        '''
        The new Customer sender is the Facility that created it
        '''
        for node in self.node_list:
            node.generator.generate_customer()
            assert(node.incoming_customers.get().sender is node)
            
    def testRandomNumberGenerator(self):
        '''
        The CustomerGenerator holds the parameters of the distribution of intertimes.
        '''
        ok_types = lambda t: (t is FunctionType) or (t is BuiltinFunctionType)
        for node in self.node_list:
            assert(node.generator.distribution is not None)
            assert (ok_types(type(node.generator.distribution)))
            assert(ok_types(type(node.service_times_distribution)))
            
    def testNumberIsGenerated(self):
        '''
        The number generator must always generate numbers
        '''
        for node in self.node_list:
            assert(isinstance(node.generator.distribution(), numbers.Number))
            assert(isinstance(node.service_times_distribution(), numbers.Number))
            
    def testCustomerSendingAndReceiving(self):
        '''
        Test the Facility can pop Customers from the Queue and send them to another Facility. 
        '''
        sender = self.node_list[0]
        receiver = self.node_list[1]
        sender.generator.generate_customer()
        customer = sender.peek_customer()
        sender.redirect_customer(sender.get_customer(), receiver)
        assert(receiver.get_customer() == customer)
        
    def testCustomerProperty(self):
        '''
        Ensure every time a Facility f gets a Customer from the queue the Customer sender corresponds to the facility itself and the receiver is not set yet.
        '''
        sender = self.node_list[0]
        receiver = self.node_list[1]
        sender.generator.generate_customer()
        sender.redirect_customer(sender.get_customer(), receiver)
        received = receiver.get_customer()
        assert(received.sender == receiver)
        assert(received.receiver is None)
        
if __name__ == "__main__":
    super.main()