'''
Created on Nov 2, 2018

@author: lucafavalli
'''
import unittest
import data_structures.network
from serialization import csv_network
import threading

class AbstractJacksonUnitTest(unittest.TestCase):
    '''
    This class defines the default setUp and tearDown Methods for all the test classes
    '''

    def setUp(self):
        '''
        Tested file is 'csv/net_csv_data/test_net_1.csv'
        '''
        unittest.TestCase.setUp(self)
        self.active_count = threading.active_count()
        self.node_list = csv_network.read_nodes_from_csv('../csv/net_csv_data/test_net_1.csv')
        self.network = data_structures.network.Network(self.node_list)