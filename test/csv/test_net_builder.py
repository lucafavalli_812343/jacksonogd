'''
Created on Oct 19, 2018

@author: lucafavalli
'''
from test.jackson_ogd_test import AbstractJacksonUnitTest
from data_structures import network
from serialization import csv_network
import data_structures

class TestNetBuilder(AbstractJacksonUnitTest):
    '''
    Unit Test Class for reading csv data_structures descriptors and build the corresponding data_structures.
    
    Required Features:
    - Node building
    - Node connections
    - Graph building
    - Graph traversal
    '''
    
    def testNodeBuilding(self):
        '''
        Test a csv file is read correctly to create a NetworkNode object
        '''
        assert(self.node_list[0].parameters == [0.5])
        assert(self.node_list[1].parameters == [0.5])
        assert(self.node_list[0].edges[0:3] == [0, 0.3, 0.7])
        assert(self.node_list[1].edges[0:3] == [0.3, 0.7, 0])
    
    def testNoDuplicateIds(self):
        '''
        Test csv does not generate a NetworkNode list with duplicate facility_ids
        '''
        assert(len(self.node_list) == len(set(self.node_list)))
        
    def testFillWithZeroes(self):
        '''
        edges list of each NetworkNode should be as long as the total number of NetworkNodes in the Network
        '''
        for node in self.node_list:
            assert(len(node.edges) == len(self.node_list))
            
    def testNetworkIdentity(self):
        '''
        Create a NodesNetwork and verify that each NetworkNode in the Network references it
        '''
        for facility in self.network.facilities:
            assert(facility.network is self.network)
            
    def testNodesAreReacheble(self):
        '''
        Test reachability of a Node from another node
        '''
        for facility in self.network.facilities:
            for i in range(len(self.network.facilities)): 
                next_facility = facility.get_facility(i)
                assert(facility.edges[i] == next_facility['weight'])
                assert(self.network.facilities[i] is next_facility['facility'])                
    
    def testMatrix(self):
        '''
        Test the network can generate an edges matrix with the correct weight
        ''' 
        matrix = self.network.weights_matrix()
        shp = matrix.shape
        for i in range(shp[0]):
            for j in range(shp[1]):
                assert(matrix[i, j] == self.network.facilities[i].edges[j])
        
    def testGenerateParameters(self):
        '''
        Test the generation of parameters for a new Network
        '''
        l = network.generate(5, 10)
        assert(len(l) == 10)
        for elem in l:
            assert(len(elem[0]) == 5)
            assert(len(elem[1]) == 10)
            for n in elem[0]:
                assert(int(n * 100) == n*100)
            for n in elem[1]:
                assert(int(n * 100) == n*100)
     
    def testNewNetworkCsv(self):
        '''
        Test you can save a generated netork to csv and load it back
        '''
        directory = '../csv/net_csv_data'
        filename = csv_network.serialize_network(network.generate(5, 10), directory)
        self.node_list = csv_network.read_nodes_from_csv(directory + '/' + filename)
        self.network = data_structures.network.Network(self.node_list)
        self.testNodesAreReacheble()
        self.testMatrix()
        
if __name__ == "__main__":
    super.main()