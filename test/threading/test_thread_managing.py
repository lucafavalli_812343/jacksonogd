'''
Created on Oct 26, 2018

@author: lucafavalli
'''
import threading
from test.jackson_ogd_test import AbstractJacksonUnitTest

class TestThreadManaging(AbstractJacksonUnitTest):
    '''
    Unit Test Class for creating a thread for each Facility and managing the resulting behavior.
    
    Required Features:
    - Threads Creation
    - Threads Start
    - Concurrent access to resources
    - Waiting service times
    
    NOT WORKING WITH FINAL VERSION OF THREADS
    '''
    
    #===========================================================================
    # def setUp(self):
    #     AbstractJacksonUnitTest.setUp(self)
    #     self.network.start()   
    # 
    # def tearDown(self):
    #     '''
    #     Stop the network
    #     '''
    #     self.network.stop_gently()
    #===========================================================================

    def testFacilitiesAreThreads(self):
        '''
        Test Facilities are instances of Thread
        '''
        for node in self.node_list:
            assert(issubclass(type(node), threading.Thread))
            
    def testGeneratorsAreThreads(self):
        '''
        Test Generators are also instances of Thread
        '''
        for node in self.node_list:
            assert(issubclass(type(node.generator), threading.Thread))
    
    #===========================================================================
    # def testNumberOfThreads(self):
    #     '''
    #     Test that for n Facilies at least 2n threads are created
    #     Also, the threads should be stoppable (gently)
    #     '''
    #     try:
    #         assert(self.network.running)
    #     except AttributeError:
    #         assert(False)
    #     assert(threading.active_count() - self.active_count >= 2 * len(self.node_list))
    #===========================================================================
          
if __name__ == "__main__":
    super.main()