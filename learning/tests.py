from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn.linear_model import SGDClassifier

import numpy as np

def run(train_x, train_y, test_x, test_y):
    '''
    logreg = LogisticRegression()
    logreg.fit(train_x, train_y)
    print('Accuracy of Logistic Regression classifier on training set: {:.2f}'.format(logreg.score(train_x, train_y)))
    print('Accuracy of Logistic Regression classifier on test set: {:.2f}'.format(logreg.score(test_x, test_y)))
    
    clf = DecisionTreeClassifier()
    clf.fit(train_x, train_y)
    print('Accuracy of Decision Tree classifier on training set: {:.2f}'.format(clf.score(train_x, train_y)))
    print('Accuracy of Decision Tree classifier on test set: {:.2f}'.format(clf.score(test_x, test_y)))
    
    knn = KNeighborsClassifier()
    knn.fit(train_x, train_y)
    print('Accuracy of K-Nearest Neighbors classifier on training set: {:.2f}'.format(knn.score(train_x, train_y)))
    print('Accuracy of K-Nearest Neighbors classifier on test set: {:.2f}'.format(knn.score(test_x, test_y)))
    '''
    
    clf = SGDClassifier(loss="hinge", penalty="l2", max_iter=5)
    clf.partial_fit(train_x, train_y, classes = np.unique(np.append(train_y, test_y)))
    print('Accuracy of classifier on training set: {:.2f}'.format(clf.score(train_x, train_y)))
    print('Accuracy of classifier on test set: {:.2f}'.format(clf.score(test_x, test_y)))