'''
Created on Oct 26, 2018

@author: lucafavalli
'''

class Customer(object):
    '''
    The customers populating the Network are instances of the Customer class.
    Each Customer brings specific information used by the network to deal with it and the time spent inside the Network
    '''


    def __init__(self, customer_id, content, sender = None, receiver = None):
        '''
        Initialize the a Customer object.
        
        Parameters
        ----------
        customer_id: int
            Unique identifier for the new customer
        content : list
            list of all the information brought by the Customer
        sender : Facility
            facility the Customer was served by in the last step
        receiver : Facility
            facility that will be serving the Customer in the next step (it will always correspond to the current facility)
        '''
        self.customer_id = customer_id
        self.content = content
        self.sender = sender
        self.receiver = receiver
        self.age = 0